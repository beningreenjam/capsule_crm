<?php

namespace Smac925\CapsuleCrm;

use Illuminate\Support\ServiceProvider;

class CapsuleCrmServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Capsule', function() {
            return new Capsule;
        });
    }
}
