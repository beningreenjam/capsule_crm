<?php namespace Smac925\CapsuleCrm;

use Illuminate\Support\Facades\Facade;

class CapsuleCrmFacade extends Facade {

    protected static function getFacadeAccessor()
    {
        return 'Capsule';
    }

}