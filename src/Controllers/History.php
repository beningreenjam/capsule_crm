<?php namespace Smac925\CapsuleCrm\Controllers;

final class History extends BaseController {

    protected $path = 'history';

    public function index($query = null)
    {
        $this->api->path( $this->path );

        if(!is_array($query)) {
            $this->api->setQueryParameters($query);
        }

        $this->request();

        return $this->api->result();
    }

    public function get($id)
    {
        $this->api->path( "{$this->path}/{$id}" );
        $this->request();

        return $this->api->result();
    }

    public function add($data, $dataType = 'json')
    {
        $this->api->path( $this->path );

        ($dataType == 'json') ? $this->api->setJson($data) : $this->api->setXml($data);

        $this->request('POST');

        if($this->api->status() == 201) {
            $location = explode('/',$this->api->getHeader('Location'));
            return (int) end($location);
        }

        return false;
    }

    public function update( $id, $history, $dataType)
    {
        $this->api->path( "{$this->path}/{$id}" );

        ($dataType == 'json') ? $this->api->setJson($history) : $this->api->setXml($history);

        $this->request('PUT');

        return ($this->api->status() == 200);
    }

    public function delete($id)
    {
        $this->api->path( "{$this->path}/{$id}" );
        $this->request('DELETE');

        return ($this->api->status() == 200);
    }

    public function addCase($partyID, $data, $dataType = 'json')
    {
        $this->path = "party/{$partyID}/history";

        return $this->add($data, $dataType);
    }

    public function partyCases($partyID)
    {
        $this->path = "party/{$partyID}/history";

        return $this->index();
    }

}