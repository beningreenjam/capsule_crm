<?php namespace Smac925\CapsuleCrm\Controllers;

use Exception;
use Smac925\Api\ApiWrapper;
use Smac925\Api\Exception\ConfigException;

abstract class BaseController {

    protected $api;
    protected $path = '';

    public function __construct()
    {
        $this->api = new ApiWrapper;
        $this->api->throttle_limit = 0;
        $this->api->throttle_time = 0;
        $this->api->config('capsulecrm');
        $this->api->setHeader('Authorization', 'Basic ' . config("services.capsulecrm.secret"));
    }

    public function setReturnType($type)
    {
        $this->api->setHeader('Accept', "application/{$type}");

        return $this;
    }

    protected function request($method = 'GET')
    {
        try {
            return $this->api->request($method);
        } catch(ConfigException $e) {
            abort(500, $e->getMessage());
        } catch(Exception $e) {
            abort($e->getCode(), $e->getMessage());
        }
    }

    protected function result()
    {
        return $this->api->result();
    }

    protected function store($transformed = null, $expire)
    {
        return $this->api->store($transformed, $expire);
    }

    public function listTags( $resourceId ) {
        $this->api->path( "{$this->path}/{$resourceId}/tag" );
        $this->request();

        return $this->api->result();
    }

    public function addTag( $resourceId, $tagName ) {
        $this->api->path( "{$this->path}/{$resourceId}/tag/{$tagName}" );
        $this->request('POST');

        return $this->api->result();
    }

    public function removeTag( $resourceId, $tagName ) {
        $this->api->path( "{$this->path}/{$resourceId}/tag/{$tagName}" );
        $this->request('DELETE');

        return $this->api->result();
    }

}