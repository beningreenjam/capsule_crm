<?php namespace Smac925\CapsuleCrm\Controllers;

final class Party extends BaseController {

    protected $path = 'party';

    public function people($organisationID)
    {
        $this->path .= "/{$organisationID}/people";

        return $this->index();
    }

    public function addPerson($data, $dataType = 'json')
    {
        $this->path = 'person';

        return $this->add($data, $dataType);
    }

    public function updatePerson($id, $data, $dataType = 'json')
    {
        $this->path = 'person';

        return $this->update($id, $data, $dataType);
    }

    public function removeFromOrganisation($organisationID, $personID)
    {
        $this->api->path( "{$this->path}/{$organisationID}/contact/{$personID}" );
        $this->request('DELETE');

        return ($this->api->status() == 200);
    }

    public function addOrganisation($data, $dataType = 'json')
    {
        $this->path = 'organisation';

        return $this->add($data, $dataType);
    }

    public function updateOrganisation($id, $data, $dataType = 'json')
    {
        $this->path = 'organisation';

        return $this->update($id, $data, $dataType);
    }

    public function index($query = null)
    {
        $this->api->path( $this->path );

        if(is_array($query)) {
            $this->api->setQueryParameters($query);
        }

        $this->request();

        return $this->api->result();
    }

    public function get($id)
    {
        $this->api->path( "{$this->path}/{$id}" );
        $this->request();

        return $this->api->result();
    }

    public function add($data, $dataType = 'json')
    {
        $this->api->path( $this->path );

        ($dataType == 'json') ? $this->api->setJson($data) : $this->api->setXml($data);

        $this->request('POST');

        if($this->api->status() == 201) {
            $location = explode('/',$this->api->getHeader('Location'));
            return (int) end($location);
        }

        return false;
    }

    public function delete($id)
    {
        $this->api->path( "{$this->path}/{$id}" );
        $this->request('DELETE');

        return ($this->api->status() == 200);
    }

    private function update($id, $party, $dataType)
    {
        $this->api->path( "{$this->path}/{$id}" );

        ($dataType == 'json') ? $this->api->setJson($party) : $this->api->setXml($party);

        $this->request('PUT');

        return ($this->api->status() == 200);
    }

}