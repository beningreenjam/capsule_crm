<?php namespace Smac925\CapsuleCrm;

use Smac925\CapsuleCrm\Controllers\Cases;
use Smac925\CapsuleCrm\Controllers\History;
use Smac925\CapsuleCrm\Controllers\Party;

class Capsule {

    public function cases( $returnType = 'json' ) {
        $case = new Cases;
        $case->setReturnType( $returnType );

        return $case;
    }

    public function history( $returnType = 'json' ) {
        $history = new History;
        $history->setReturnType( $returnType );

        return $history;
    }

    public function party( $returnType = 'json' ) {
        $party = new Party;
        $party->setReturnType( $returnType );

        return $party;
    }

}
